window.addEventListener('DOMContentLoaded', async () => {
    try {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const selectTag = document.getElementById('state');
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));

                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newLocation = await response.json();
                    console.log(newLocation);
                }
            });
            
            data.states.forEach(state => {
                selectTag.innerHTML += `
                <option value="${state.abbreviation}">${state.name}</option>
                `;
            });



        } else {
            throw (new Error("Response Error", response));
        }
    }   catch (err) {
        console.log(err);
    }
});
