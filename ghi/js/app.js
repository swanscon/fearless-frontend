function createCard(name, description, pictureUrl, starts, ends, location) {
  const startDate = new Date(starts);
  const startMonth = startDate.getUTCMonth()+1;
  const startDay = startDate.getUTCDate();
  const startYear = startDate.getFullYear();
  const endDate = new Date(ends);
  const endMonth = endDate.getUTCMonth()+1;
  const endDay = endDate.getUTCDate();
  const endYear = endDate.getFullYear();
  return `
    <div class="col-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <p class="card-header text-center">
        ${startMonth}/${startDay}/${startYear} - ${endMonth}/${endDay}/${endYear}
        </p>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      // Figure out what to do when the response is bad
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const html = createCard(name, description, pictureUrl, starts, ends, location);
          const column = document.querySelector('.row');
          column.innerHTML += html;
        }
        
      }

    }
  } catch (e) {
    console.error(e);
    // Figure out what to do if an error is raised
  }

});