window.addEventListener('DOMContentLoaded', async () => {
    try {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const selectTag = document.getElementById('conference');
            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const selectId = document.getElementById('conference').value;
                const presentationUrl = `http://localhost:8000/api/conferences/${selectId}/presentations/`;
                console.log(presentationUrl);
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(presentationUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newPresentation = await response.json();
                    console.log(newPresentation);
                };
            });     
            data.conferences.forEach(conference => {
                selectTag.innerHTML += `
                <option value="${conference.id}">${conference.name}</option>
                `;
            });

        } else {
            throw (new Error("Response Error", response));
        }
    }   catch (err) {
        console.log(err);
    }
});
