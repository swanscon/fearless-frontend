window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    try {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const formTag = document.getElementById('create-attendee-form');
            
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            };

            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
 
                const attendeeUrl = `http://localhost:8001${selectTag.value}attendees/`;

                console.log(attendeeUrl);
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(attendeeUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                    const newAttendee = await response.json();
                    console.log(newAttendee);
                    const showSuccess = document.getElementById('success-message');
                    showSuccess.classList.remove('d-none');
                    formTag.classList.add('d-none');
                };
            });



            const hideLoading = document.getElementById('loading-conference-spinner');
            hideLoading.classList.add('d-none');
            selectTag.classList.remove('d-none');

        } else {
            throw (new Error("Response Error", response));
        }

    } catch (err) {
        console.log(err);
    }
    
});

// window.addEventListener('DOMContentLoaded', async () => {
//     try {   
//         const url = 'http://localhost:8000/api/conferences/';
//         const response = await fetch(url);
//         if (response.ok) {
//             const data = await response.json();
//             const selectTag = document.getElementById('conference');
//             const formTag = document.getElementById('create-attendee-form');
//             formTag.addEventListener('submit', async event => {
//                 event.preventDefault();
//                 const formData = new FormData(formTag);
//                 const json = JSON.stringify(Object.fromEntries(formData));
//                 const attendeeUrl = `http://localhost:8000/api/attendees/`;
//                 console.log(attendeeUrl);
//                 const fetchConfig = {
//                     method: "post",
//                     body: json,
//                     headers: {
//                         'Content-Type': 'application/json',
//                     },
//                 };
//                 const response = await fetch(attendeeUrl, fetchConfig);
//                 if (response.ok) {
//                     formTag.reset();
//                     const newAttendee = await response.json();
//                     console.log(newAttendee);
//                 };
//             });
//             for (let conference of data.conferences) {
//                 const option = document.createElement('option');
//                 option.value = conference.href;
//                 option.innerHTML = conference.name;
//                 selectTag.appendChild(option);
//             };
//             const hideLoading = document.getElementById('loading-conference-spinner');
//             hideLoading.classList.add('d-none');
//             selectTag.classList.remove('d-none');
//         } else {
//             throw (new Error("Response Error", response));
//             }
// } catch (err) {
//     console.log(err);
    
// }});