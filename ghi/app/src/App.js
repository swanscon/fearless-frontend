import React from 'react';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import HomePage from './HomePage';
import PresentationForm from './PresentationForm';

import {
  BrowserRouter,
  Routes,
  Route,
} from 'react-router-dom';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<HomePage />}/>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees}/>}/>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm/>}/>
          </Route>
          <Route path="locations">
            <Route path="new" element={<LocationForm/>}/>
          </Route>
          <Route path="attendees/new" element={<AttendConferenceForm/>}/>
          <Route path="presentations/new" element={<PresentationForm/>}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;