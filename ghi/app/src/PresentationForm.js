import React from 'react';

class PresentationForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            presenterName: '',
            presenterEmail: '',
            companyName: '',
            title: '',
            synopsis: '',
            conference: '',
            conferences: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangeCompanyName = this.handleChangeCompanyName.bind(this);
        this.handleChangeTitle = this.handleChangeTitle.bind(this);
        this.handleChangeSynopsis = this.handleChangeSynopsis.bind(this);
        this.handleChangeConference = this.handleChangeConference.bind(this);
    }

    handleChangeName(e) {
        const value = e.target.value;
        this.setState({ presenterName: value });
    }
    handleChangeEmail(e) {
        const value = e.target.value;
        this.setState({ presenterEmail: value });
    }
    handleChangeCompanyName(e) {
        const value = e.target.value;
        this.setState({ companyName: value });
    }
    handleChangeTitle(e) {
        const value = e.target.value;
        this.setState({ title: value });
    }
    handleChangeSynopsis(e) {
        const value = e.target.value;
        this.setState({ synopsis: value });
    }
    handleChangeConference(e) {
        const value = e.target.value;
        this.setState({ conference: value });
        console.log(this.state.conference);
    }

    async handleSubmit(e) {
        e.preventDefault();
        const data = {...this.state};
        data.presenter_name = data.presenterName;
        data.presenter_email = data.presenterEmail;
        data.company_name = data.companyName;
        delete data.presenterName;
        delete data.presenterEmail;
        delete data.companyName;
        delete data.conferences;

        const presentationUrl = `http://localhost:8000/api/conferences/{conference.id}/presentations/`;
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);
            this.setState({
                presenter_name: '',
                presenter_email: '',
                company_name: '',
                title: '',
                synopsis: '',
                conference: '',
            });
        }
    }

    async componentDidMount() {
        try{
            const url = 'http://localhost:8000/api/conferences/'
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                this.setState({ conferences: data.conferences })
            } else{
                throw (new Error("Response Error", response));
            }
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
        <div className="row">
            <div className="offset-3 col-7">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form onSubmit={this.handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeName} placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" value={this.state.presenterName}/>
                            <label htmlFor="name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeEmail} placeholder="Presenter email" required type="text" name="presenter_email" id="presenter_email" className="form-control" value={this.state.presenterEmail}/>
                            <label htmlFor="name">Presenter email</label>
                        </div>              
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeCompanyName} placeholder="Company name" required type="text" name="company_name" id="company_name" className="form-control" value={this.state.companyName}/>
                            <label htmlFor="name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangeTitle} placeholder="Title" required type="text" name="title" id="title" className="form-control" value={this.state.title}/>
                            <label htmlFor="name">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea  onChange={this.handleChangeSynopsis} name="synopsis" id="synopsis" className="form-control" rows="3" value={this.state.synopsis}>
                            </textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChangeConference} required id="conference" name="conference" className="form-select" value={this.state.conference}>
                            <option>Choose a conference</option>
                            {this.state.conferences.map(conference => {
                                return (
                                    <option key={conference.id} value={conference.id}>
                                        {conference.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        );
    }
}

export default PresentationForm;